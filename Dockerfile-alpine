# https://hub.docker.com/_/nginx
ARG BASE_VERSION
FROM nginx:${BASE_VERSION:-alpine}

MAINTAINER Marek Rychly <marek.rychly@gmail.com>

ENV \
WEBROOT_DIR=/usr/share/nginx/html/ta

COPY scripts /

COPY dist /tmp

RUN true \
# make the scripts executable
&& chmod 755 /*.sh \
# curl: to initialize repositories by calling RDF4J-Workbench and to check health status by querying the web-page
&& apk add --no-cache --update curl \
# wait-for: the repository init script needs to wait for a proxied web-server to call RDF4J-Workbench
&& wget -O /usr/bin/wait-for "https://raw.githubusercontent.com/eficode/wait-for/master/wait-for" && chmod 755 /usr/bin/wait-for \
\
# copy OWL and extract a web-app into the static html directory
&& cp -v /tmp/*.owl / \
&& mkdir -p "${WEBROOT_DIR}" \
&& tar -xzf /tmp/*.tar.gz -C "${WEBROOT_DIR}" --strip-components 1 \
\
# enable customized Nginx (re)configuration files (or their templates)
&& cp -v /tmp/*.conf* /etc/nginx/conf.d/ \
\
# clean up
&& rm -rf /tmp/* /var/tmp/* /var/cache/apk/*

ENTRYPOINT ["/entrypoint.sh"]

HEALTHCHECK CMD /healthcheck.sh
