#!/bin/sh

for I in /etc/nginx/conf.d/*.template; do
	# envsubst does not support shell-like variable patterns, so the default values will be set here
	NGINX_PORT=${NGINX_PORT:-80} \
	RDF4J_SERVER_HOST=${RDF4J_SERVER_HOST:-localhost} \
	RDF4J_SERVER_PORT=${RDF4J_SERVER_PORT:-80} \
	RDF4J_WORKBENCH_HOST=${RDF4J_WORKBENCH_HOST:-localhost} \
	RDF4J_WORKBENCH_PORT=${RDF4J_WORKBENCH_PORT:-80} \
	envsubst <${I} >${I%.template}
done

[ -z "${SKIP_INIT}" ] && /init.sh --stamp=/init.done &

exec nginx -g 'daemon off;' ${@}
