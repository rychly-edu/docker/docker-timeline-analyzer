# tags (before "\t") and build arguments (after "\t", separated by " " which cannot be utilised otherwise) of the Docker image variants to build
# for versions, see https://hub.docker.com/_/nginx?tab=description
nginx1.17	BASE_VERSION=1.17
